<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit685a2de71028057db63208833e84fdbf
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP148423',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit685a2de71028057db63208833e84fdbf::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit685a2de71028057db63208833e84fdbf::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
