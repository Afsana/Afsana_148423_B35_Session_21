<?php
namespace App\BookTitle;

use App\Model\Database as DB;
use PDO;

require_once("../../../../vendor/autoload.php");

class BookTitle extends DB{
    public $id;
    public $book_title; // property name all time small letter
    public $author_name;

    public function __construct()
    {
        parent::__construct();
    }
}
$objBookTitle=new BookTitle;
?>