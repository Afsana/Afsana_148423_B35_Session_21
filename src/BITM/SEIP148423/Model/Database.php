<?php
namespace App\Model;

use PDO;
use PDOException;

//print_r(PDO::getAvailableDrivers()); // :: this is a scope resulation

class Database
{

    public $DBH;
    public $host = "localhost";
    public $dbname="atomic_project_b35";
    public $user="root";
    public $password="";

    public function __construct() //for auto call we write code in the construct function
    {
        try {
            # MySQL with PDO_MYSQL
            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password); // mysql thake start kore dbname" porjonto purata string & obossoi double quoted er moddhe ralkte hobe.
            echo "Connected Successfully!";
        }
        catch (PDOException $e) { // here PDOException is built in & also previous NEW PDO er PDO holo built in
            echo $e->getMessage();
        }
    }
}

//$objDatabase = new Database(); // if we writ this then in the BookTitle code (when it is run) we get output double output(connected successfully!)
?>

